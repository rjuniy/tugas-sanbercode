@extends('layout.master')

@section('judul')
Halaman Utama
@endsection

@section('content')
    
    <h1>Selamat Datang {{$firstName}} {{$lastName}}!</h1>
    <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
@endsection
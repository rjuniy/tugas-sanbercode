<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Register</title>
</head>
<body>
  <h1>Buat Akun Baru!</h1>
  <h3>Sign Up Form</h3>
  <form action="/welcome" method="POST">
    @csrf
    <label>Full Name:</label><br>
    <input type="text" name="fname"><br><br>
    <label>Last Name:</label><br><br>
    <input type="text" name="lname"><br><br>
    <label>Gender:</label><br><br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br>
    <input type="radio" name="gender">Other<br><br>
    <label>Nationality:</label><br><br>
    <select name="nationality" id="">
      <option value="indonesia">Indonesia</option>
    </select><br><br>
    <label for="">Language Spoken:</label><br><br>
    <input type="checkbox" name="" id="">Bahasa Indonesia</input><br>
    <input type="checkbox" name="" id="">English</input><br>
    <input type="checkbox" name="" id="">Other</input><br><br>
    <label for="">Bio:</label><br><br>
    <textarea name="bio"></textarea><br><br>
    <input type="submit" value="Sign Up">
  </form>
</body>
</html>
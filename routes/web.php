<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;

Route::get('/', [HomeController::class, 'home']);
Route::get('/welcome', [HomeController::class, 'welcome']);
Route::get('/register', [AuthController::class, 'register'])->name('register');
Route::post('/welcome', [AuthController::class, 'signup']);
Route::get('/table', function(){
  return view('halaman.table');
});
Route::get('/data-tables' , function(){
  return view('halaman.data-tables');
});